#DNS Benchmark
This tool is test a number of public DNS resolvers to tell the user which are the fastest. It is still a work in progress, but the main branch is stable...

#Setup
##Requirements
* Python
* Python-pip
* [dnslib](https://pypi.python.org/pypi/dnslib)

##Ubuntu
    apt-get install python python-pip git
    git clone https://bitbucket.org/chpwssn/dns-benchmark.git
    pip install -r dns-benchmark/requirements.txt
    
##OSX
	git clone https://bitbucket.org/chpwssn/dns-benchmark.git
    pip install -r dns-benchmark/requirements.txt
    
#Usage
    python benchmark.py
    
The script will then run the tests and average the response times, then give the results ordered by increasing average response time.

    -- Results --

     OpenNIC
	    Problem: None
	    Best address: 96.90.175.167
	    Average Response Time: 0.00905863841375 sec
    	Successes: 50
	    Success ratio: 60
    Norton DNS
    	Problem:  Possible hijacking
    	Best address: 198.153.192.1
    	Average Response Time: 0.0285657246908 sec
    	Successes: 60
    	Success ratio: 60
    	...
    	
## Command Line Arguments
    -v          verbose output
    --opennic      Test closest OpenNIC servers
    --all-opennic  Test all OpenNIC servers
## Example output
You can see example output in `example-output.txt`
    