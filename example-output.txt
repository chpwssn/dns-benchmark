Finished loading Alternate DNS
Finished loading BlockAid Public DNS (or PeerDNS)
Finished loading Censurfridns
Finished loading Comodo Secure DNS
Finished loading DNS.Watch
Finished loading DNSReactor
Finished loading Dyn
Finished loading FDN
Finished loading FoolDNS
Finished loading FreeDNS
Finished loading Google Public DNS
Finished loading GreenTeamDNS
Finished loading Hurricane Electric
Finished loading Level3
Finished loading Neustar DNS Advantage
Finished loading New Nations
Finished loading Norton DNS
Finished loading OpenDNS
Finished loading PowerNS
Finished loading SafeDNS
Finished loading SkyDNS
Finished loading SmartViper Public DNS
Finished loading Verisign
Finished loading Xiala.net
Finished loading Yandex.DNS
Finished loading puntCAT
Testing Alternate DNS
	Testing: 198.101.242.72
		average: 0.0457655566079 sec, successes: 35, failures: 25
	Testing: 23.253.163.53
		average: 0.0457655566079 sec, successes: 35, failures: 25
	Alternate DNS average: 0.0457655566079 sec, successes: 35, success ratio: 1
Testing BlockAid Public DNS (or PeerDNS)
	Testing: 205.204.88.60
		This server is responding to OpenNIC domains... it should not...
		average: 0.122819807794 sec, successes: 54, failures: 6
	Testing: 178.21.23.150
		This server is responding to OpenNIC domains... it should not...
		average: 0.122819807794 sec, successes: 60, failures: 0
	BlockAid Public DNS (or PeerDNS) average: 0.122819807794 sec, successes: 60, success ratio: 60
Testing Censurfridns
	Testing: 91.239.100.100
		average: 0.176374271938 sec, successes: 35, failures: 25
	Testing: 89.233.43.71
		average: 0.176374271938 sec, successes: 35, failures: 25
	Censurfridns average: 0.176374271938 sec, successes: 35, success ratio: 1
Testing Comodo Secure DNS
	Testing: 8.26.56.26
		This server is responding to OpenNIC domains... it should not...
		average: 0.0606579533939 sec, successes: 58, failures: 2
	Testing: 8.20.247.20
		This server is responding to OpenNIC domains... it should not...
		average: 0.0585410435994 sec, successes: 60, failures: 0
	Comodo Secure DNS average: 0.0585410435994 sec, successes: 60, success ratio: 60
Testing DNS.Watch
	Testing: 84.200.69.80
		average: 0.19788609129 sec, successes: 33, failures: 27
	Testing: 84.200.70.40
		average: 0.1942184312 sec, successes: 35, failures: 25
	DNS.Watch average: 0.1942184312 sec, successes: 35, success ratio: 1
Testing DNSReactor
	Testing: 104.236.210.29
		This server is responding to OpenNIC domains... it should not...
		average: 0.110374742541 sec, successes: 58, failures: 2
	Testing: 45.55.155.25
		This server is responding to OpenNIC domains... it should not...
		average: 0.108152074329 sec, successes: 59, failures: 1
	DNSReactor average: 0.108152074329 sec, successes: 59, success ratio: 59
Testing Dyn
	Testing: 216.146.35.35
		This server is responding to OpenNIC domains... it should not...
		average: 0.0660120010376 sec, successes: 60, failures: 0
	Testing: 216.146.36.36
		This server is responding to OpenNIC domains... it should not...
		average: 0.0660120010376 sec, successes: 59, failures: 1
	Dyn average: 0.0660120010376 sec, successes: 59, success ratio: 60
Testing FDN
	Testing: 80.67.169.12
		average: 0.185421522926 sec, successes: 34, failures: 26
	FDN average: 0.185421522926 sec, successes: 34, success ratio: 1
Testing FoolDNS
	Testing: 87.118.111.215
		average: 0.16925041335 sec, successes: 35, failures: 25
	Testing: 213.187.11.62
		average: 0.16925041335 sec, successes: 0, failures: 60
	FoolDNS average: 0.16925041335 sec, successes: 0, success ratio: 1
Testing FreeDNS
	Testing: 37.235.1.174
		average: 0.180891124407 sec, successes: 30, failures: 30
	Testing: 37.235.1.177
		average: 0.180891124407 sec, successes: 31, failures: 29
	FreeDNS average: 0.180891124407 sec, successes: 31, success ratio: 1
Testing Google Public DNS
	Testing: 8.8.8.8
		average: 0.0584029212142 sec, successes: 33, failures: 27
	Testing: 8.8.4.4
		average: 0.0480783530644 sec, successes: 35, failures: 25
	Google Public DNS average: 0.0480783530644 sec, successes: 35, success ratio: 1
Testing GreenTeamDNS
	Testing: 81.218.119.11
		This server is responding to OpenNIC domains... it should not...
		average: 0.246284331306 sec, successes: 59, failures: 1
	Testing: 209.88.198.133
		This server is responding to OpenNIC domains... it should not...
		average: 0.239908400449 sec, successes: 55, failures: 5
	GreenTeamDNS average: 0.239908400449 sec, successes: 55, success ratio: 59
Testing Hurricane Electric
	Testing: 74.82.42.42
		average: 0.0476701123374 sec, successes: 35, failures: 25
	Hurricane Electric average: 0.0476701123374 sec, successes: 35, success ratio: 1
Testing Level3
	Testing: 209.244.0.3
		This server is responding to OpenNIC domains... it should not...
		average: 0.0504699239024 sec, successes: 54, failures: 6
	Testing: 209.244.0.4
		This server is responding to OpenNIC domains... it should not...
		average: 0.0504699239024 sec, successes: 55, failures: 5
	Level3 average: 0.0504699239024 sec, successes: 55, success ratio: 11
Testing Neustar DNS Advantage
	Testing: 156.154.70.1
		This server is responding to OpenNIC domains... it should not...
		average: 0.0497149653354 sec, successes: 59, failures: 1
	Testing: 156.154.71.1
		This server is responding to OpenNIC domains... it should not...
		average: 0.0497149653354 sec, successes: 59, failures: 1
	Neustar DNS Advantage average: 0.0497149653354 sec, successes: 59, success ratio: 59
Testing New Nations
	Testing: 5.45.96.220
		This server is responding to OpenNIC domains... it should not...
		average: 0.156634339264 sec, successes: 56, failures: 4
	Testing: 185.82.22.133
		This server is responding to OpenNIC domains... it should not...
		average: 0.156634339264 sec, successes: 58, failures: 2
	New Nations average: 0.156634339264 sec, successes: 58, success ratio: 29
Testing Norton DNS
	Testing: 198.153.192.1
		This server is responding to OpenNIC domains... it should not...
		average: 0.0352680007617 sec, successes: 60, failures: 0
	Testing: 198.153.194.1
		This server is responding to OpenNIC domains... it should not...
		average: 0.0352680007617 sec, successes: 60, failures: 0
	Norton DNS average: 0.0352680007617 sec, successes: 60, success ratio: 60
Testing OpenDNS
	Testing: 208.67.222.222
		average: 0.0406758649009 sec, successes: 35, failures: 25
	Testing: 208.67.220.220
		average: 0.039079454967 sec, successes: 35, failures: 25
	OpenDNS average: 0.039079454967 sec, successes: 35, success ratio: 1
Testing PowerNS
	Testing: 194.145.226.26
		average: 999 sec, successes: 0, failures: 60
	Testing: 77.220.232.44
		average: 999 sec, successes: 0, failures: 60
	PowerNS average: 999 sec, successes: 0, success ratio: 0
Testing SafeDNS
	Testing: 195.46.39.39
		average: 0.169321487932 sec, successes: 34, failures: 26
	Testing: 195.46.39.40
		average: 0.169321487932 sec, successes: 35, failures: 25
	SafeDNS average: 0.169321487932 sec, successes: 35, success ratio: 1
Testing SkyDNS
	Testing: 193.58.251.251
		average: 0.206440730528 sec, successes: 33, failures: 27
	SkyDNS average: 0.206440730528 sec, successes: 33, success ratio: 1
Testing SmartViper Public DNS
	Testing: 208.76.50.50
		average: 999 sec, successes: 0, failures: 60
	Testing: 208.76.51.51
		average: 999 sec, successes: 0, failures: 60
	SmartViper Public DNS average: 999 sec, successes: 0, success ratio: 0
Testing Verisign
	Testing: 64.6.64.6
		average: 0.0748513902937 sec, successes: 35, failures: 25
	Testing: 64.6.65.6
		average: 0.0509118965694 sec, successes: 35, failures: 25
	Verisign average: 0.0509118965694 sec, successes: 35, success ratio: 1
Testing Xiala.net
	Testing: 77.109.148.136
		average: 0.202226655237 sec, successes: 29, failures: 31
	Testing: 77.109.148.137
		average: 0.165278579878 sec, successes: 23, failures: 37
	Xiala.net average: 0.165278579878 sec, successes: 23, success ratio: 0
Testing Yandex.DNS
	Testing: 77.88.8.88
		average: 0.180202918894 sec, successes: 34, failures: 26
	Testing: 77.88.8.2
		average: 0.180202918894 sec, successes: 35, failures: 25
	Yandex.DNS average: 0.180202918894 sec, successes: 35, success ratio: 1
Testing puntCAT
	Testing: 109.69.8.51
		average: 0.255312211373 sec, successes: 34, failures: 26
	puntCAT average: 0.255312211373 sec, successes: 34, success ratio: 1

-- Results --

Norton DNS
	Problem:  Possible hijacking
	Best address: 198.153.192.1
	Average Response Time: 0.0352680007617 sec
	Successes: 60
	Success ratio: 60
OpenDNS
	Problem: None
	Best address: 208.67.220.220
	Average Response Time: 0.039079454967 sec
	Successes: 35
	Success ratio: 1
Alternate DNS
	Problem: None
	Best address: 198.101.242.72
	Average Response Time: 0.0457655566079 sec
	Successes: 35
	Success ratio: 1
Hurricane Electric
	Problem: None
	Best address: 74.82.42.42
	Average Response Time: 0.0476701123374 sec
	Successes: 35
	Success ratio: 1
Google Public DNS
	Problem: None
	Best address: 8.8.4.4
	Average Response Time: 0.0480783530644 sec
	Successes: 35
	Success ratio: 1
Neustar DNS Advantage
	Problem:  Possible hijacking
	Best address: 156.154.70.1
	Average Response Time: 0.0497149653354 sec
	Successes: 59
	Success ratio: 59
Level3
	Problem:  Possible hijacking
	Best address: 209.244.0.3
	Average Response Time: 0.0504699239024 sec
	Successes: 55
	Success ratio: 11
Verisign
	Problem: None
	Best address: 64.6.65.6
	Average Response Time: 0.0509118965694 sec
	Successes: 35
	Success ratio: 1
Comodo Secure DNS
	Problem:  Possible hijacking
	Best address: 8.20.247.20
	Average Response Time: 0.0585410435994 sec
	Successes: 60
	Success ratio: 60
Dyn
	Problem:  Possible hijacking
	Best address: 216.146.35.35
	Average Response Time: 0.0660120010376 sec
	Successes: 59
	Success ratio: 60
DNSReactor
	Problem:  Possible hijacking
	Best address: 45.55.155.25
	Average Response Time: 0.108152074329 sec
	Successes: 59
	Success ratio: 59
BlockAid Public DNS (or PeerDNS)
	Problem:  Possible hijacking
	Best address: 205.204.88.60
	Average Response Time: 0.122819807794 sec
	Successes: 60
	Success ratio: 60
New Nations
	Problem:  Possible hijacking
	Best address: 5.45.96.220
	Average Response Time: 0.156634339264 sec
	Successes: 58
	Success ratio: 29
Xiala.net
	Problem: None
	Best address: 77.109.148.137
	Average Response Time: 0.165278579878 sec
	Successes: 23
	Success ratio: 0
FoolDNS
	Problem: None
	Best address: 87.118.111.215
	Average Response Time: 0.16925041335 sec
	Successes: 0
	Success ratio: 1
SafeDNS
	Problem: None
	Best address: 195.46.39.39
	Average Response Time: 0.169321487932 sec
	Successes: 35
	Success ratio: 1
Censurfridns
	Problem: None
	Best address: 91.239.100.100
	Average Response Time: 0.176374271938 sec
	Successes: 35
	Success ratio: 1
Yandex.DNS
	Problem: None
	Best address: 77.88.8.88
	Average Response Time: 0.180202918894 sec
	Successes: 35
	Success ratio: 1
FreeDNS
	Problem: None
	Best address: 37.235.1.174
	Average Response Time: 0.180891124407 sec
	Successes: 31
	Success ratio: 1
FDN
	Problem: None
	Best address: 80.67.169.12
	Average Response Time: 0.185421522926 sec
	Successes: 34
	Success ratio: 1
DNS.Watch
	Problem: None
	Best address: 84.200.70.40
	Average Response Time: 0.1942184312 sec
	Successes: 35
	Success ratio: 1
SkyDNS
	Problem: None
	Best address: 193.58.251.251
	Average Response Time: 0.206440730528 sec
	Successes: 33
	Success ratio: 1
GreenTeamDNS
	Problem:  Possible hijacking
	Best address: 209.88.198.133
	Average Response Time: 0.239908400449 sec
	Successes: 55
	Success ratio: 59
puntCAT
	Problem: None
	Best address: 109.69.8.51
	Average Response Time: 0.255312211373 sec
	Successes: 34
	Success ratio: 1
PowerNS
	Problem: None
	Best address: 194.145.226.26
	Average Response Time: 999 sec
	Successes: 0
	Success ratio: 0
SmartViper Public DNS
	Problem: None
	Best address: 208.76.50.50
	Average Response Time: 999 sec
	Successes: 0
	Success ratio: 0
