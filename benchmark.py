"""
    DNS Benchmark
    This tool is meant to tell the user the fastest DNS resolver. 

    Copyright (C) 2016  Chip Wasson

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
import time, socket, json, urllib2
from dnslib import DNSRecord,DNSQuestion,QTYPE,DNSError,RCODE
from optparse import OptionParser


#Parse command line arguments
parser = OptionParser()
parser.add_option("-v", dest="verbose",action="store_true", 
	default=False, help="verbose output")
parser.add_option("--opennic", dest="opennic",action="store_true", 
	default=False, help="Test closest OpenNIC servers")
parser.add_option("--all-opennic", dest="all_opennic",action="store_true", 
	default=False, help="Test all OpenNIC servers")

(options, args) = parser.parse_args()


#Read the providers file
with open("providers.json") as providerfile:
	providers_json_in = json.loads(providerfile.read())

#Global variables
icann_usable_domains = ["google.com","opennicproject.org", "yahoo.com", 
	"wikipedia.org","360.cn","www.abs.gov.au","biglobe.ne.jp"]
opennic_usable_domains = ["opennic.oss","grep.geek","reg.for.free",
	"reg.pirate","register.bit"]
failing_domains = ["nxdomain.chip.geek"]
providers = []

"""
	DNS_Provider object contains information about the DNS resolvers that
	we loaded from the provider's file
"""
class DNS_Provider:
	addresses = []
	best_address = "None"
	opennic_flag = False
	def __init__(self, obj):
		self.addresses = obj['ip4']
		self.provider = obj['provider']
		self.info_url = obj['info_url']
		self.opennic = obj['opennic']
	def __str__(self):
		return self.provider
	def __eq__(self,other):
		return self.average == other.average
	def __lt__(self,other):
		return self.average < other.average
	def __gt__(self,other):
		return self.average > other.average
	def getFlags(self):
		ret = ""
		if self.opennic_flag:
			ret += " Possible hijacking"
		if len(ret) == 0:
			ret = "None"
		return ret


"""
	Fetch OpenNIC T2 servers from the API and 
"""
def getT2s():
	servJSON = json.loads(urllib2.urlopen("http://digbot.nerds.io/servmap.php").read())
	t2s_fetched = []
	for server in servJSON:
		if not server['whitelist'] and server['state'] == 'norm' and len(server['v4']):
			t2s_fetched.append(server['v4'])
	return t2s_fetched

"""
	Get closest T2s by GeoIP
"""
def getT2sGeoIP(iponly=False):
	servJSON = json.loads(urllib2.urlopen("https://api.opennicproject.org/geoip/?json").read())
	if iponly:
		results = []
		for server in servJSON:
			results.append(server['ip'])
		return results
	else:
		return servJSON

"""
	Perform the DNS lookup specified and return the rcode
"""
def dig(dnsServer, queryt, questionIn):
	dnsPort = 53
	tcpQuery = False
	verbose = False
	short = False
	v6query = False
	timeoutSec = .5
	
	#Attempt the dns lookup
	try:
		question = DNSRecord(q=DNSQuestion(questionIn,getattr(QTYPE,queryt)))
		packet = bytearray(question.send(dnsServer,dnsPort,tcp=tcpQuery,
			timeout=timeoutSec,ipv6=v6query))	
		original = DNSRecord.parse(packet)
	except socket.timeout as e:
		if options.verbose:
			print "Timed out"
		return 100
	try:
		if question.rr == None:
			if options.verbose:
				print "Sorry a {0} record was not found for {1}".format(queryt,questionIn)
		else:
			rcode = original.header.get_rcode()
			if not rcode == 0:
				if options.verbose:
					print "Got a status of {0}({1}) for query {2}".format(RCODE.get(rcode),
						rcode,questionIn)
			return rcode
			if short:
				resp = original.short().split("\n")
				for line in resp:
					print line
					time.sleep(.01)
			else:
				for line in original.rr:
					print line.toZone()
				time.sleep(.01)
					
	except AttributeError as e:
		print e
		errormsg = "Sorry a {0} record was not found for {1}".format(queryt,questionIn)
		print errormsg
	try:
		print(original)
		print(original.get_a())
	except:
		pass
		
	
"""
	If the --opennic flag is set, fetch the list of current T2s and
	load them for testing
"""
if options.opennic or options.all_opennic:
	if options.all_opennic:
		servers = getT2s()
	else:
		servers = getT2sGeoIP(True)
	providers.append(DNS_Provider({
		"ip4":servers,
		"provider":"OpenNIC",
		"info_url" : "http:\/\/opennicproject.org\/",
		"opennic":True
	}))
	print "Finished loading OpenNIC"

#Create DNS Provider objects for all the providers loaded from the JSON file
for prov_in in providers_json_in:
	new_provider = DNS_Provider(prov_in)
	providers.append(new_provider)
	print "Finished loading {0}".format(new_provider.provider)

#Run tests for each provider
for provider in providers:
	print "Testing {0}".format(provider)
	#Run tests for each address for the provider
	for address in provider.addresses:
		print "\tTesting: {1}".format(provider,address)
		server_avg = 0
		successes = 0
		failures = 0
		count = 5
		#Test the ICANN domains
		for domain in icann_usable_domains:
			for x in xrange(count):
				start_time = time.time()
				retcode = dig(address, "A", domain)
				end_time = time.time()
				if retcode == 0:
					successes += 1
					server_avg += end_time - start_time
				else:
					failures += 1
				time.sleep(.01)
		#Test OpenNIC domains
		for domain in opennic_usable_domains:
			for x in xrange(count):
				start_time = time.time()
				retcode = dig(address, "A", domain)
				end_time = time.time()
				# If the provider returned a record for an OpenNIC domain, but shouldn't be...
				# that's a bad thing
				if not provider.opennic and retcode == 0:
					provider.opennic_flag = True
				if retcode == 0:
					server_avg += end_time - start_time
					successes += 1
				else:
					failures += 1
				time.sleep(.01)
		#Tell the user it shouldn't be resolving OpenNIC domains
		if provider.opennic_flag:
			print "\t\tThis server is responding to OpenNIC domains... it should not..."
		provider.successes = successes
		
		#If the resolver didn't return a result, set its average response time to 999
		if successes == 0:
			server_avg = 999
		else:
			server_avg = server_avg / successes
		#Only record the average if this is the best result
		if not hasattr(provider, 'average'):
			provider.average = server_avg
			provider.best_address = address
		if provider.average > server_avg:
			provider.average = server_avg
			provider.best_address = address
		#Record our success ratio
		if failures == 0:
			success_ratio = successes
		else:
			success_ratio = successes/failures
		if not hasattr(provider, 'success_ratio'):
			provider.success_ratio = success_ratio
		if provider.success_ratio < success_ratio:
			provider.success_ratio = success_ratio
		print "\t\taverage: {0} sec, successes: {1}, failures: {2}".format(provider.average, 
			successes, failures)
	print "\t{0} average: {1} sec, successes: {2}, success ratio: {3}".format(provider,
		provider.average,provider.successes,provider.success_ratio)
	
providers = sorted(providers)
print "\n-- Results --\n"
for result in providers:
	print result
	print "\tProblem: {0}".format(result.getFlags())
	print "\tBest address: {0}".format(result.best_address)
	print "\tAverage Response Time: {0} sec".format(result.average)
	print "\tSuccesses: {0}".format(result.successes)
	print "\tSuccess ratio: {0}".format(result.success_ratio)
